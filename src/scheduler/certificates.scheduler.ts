import { Scheduler } from '@kominal/lib-node-scheduler';
import { error, info, warn } from '@kominal/observer-node-client';
import { requestNewCertificate } from '../acme-manager';
import Certificate from '../models/certificate';
import { getDomains } from '../traefik-manager';

let blacklist: { domain: string; time: number }[] = [];

export class CertificatesScheduler extends Scheduler {
	async run(): Promise<void> {
		const time = new Date().getTime();
		blacklist = blacklist.filter((entry) => entry.time > time);

		const domains = await getDomains();

		await Certificate.updateMany({ domain: { $nin: domains } }, { renew: false });
		await Certificate.updateMany({ domain: { $in: domains } }, { renew: true });

		var today = new Date();
		const maxExpiresDate = new Date(today.getFullYear(), today.getMonth(), today.getDate() + 30);
		const newExpiresDate = new Date(today.getFullYear(), today.getMonth(), today.getDate() + 90);

		let certificateRequired: string[] = [];
		for (const domain of domains) {
			if (blacklist.find((entry) => entry.domain === domain)) {
				warn(`Ignoring '${domain}'!`);
				continue;
			}
			const certificate = await Certificate.findOne({ domain });
			if (!certificate) {
				info(`Found no certificate for '${domain}'.`);
				certificateRequired.push(domain);
			} else if (certificate.get('expires') < maxExpiresDate && certificate.get('renew') === true) {
				info(`Certificate for '${domain}' is ready for renewal.`);
				certificateRequired.push(domain);
			}
		}

		info(`Found ${domains.length} domains. ${certificateRequired.length} need a new certificate. (${domains})`);

		const certificates: any[] = [];
		for (const domain of certificateRequired) {
			info(`Requesting certificate for '${domain}'...`);
			let certificate = await requestNewCertificate(domain);
			if (certificate != null) {
				info(`Received new certificate for '${domain}'.`);
				certificates.push({ domain, ...certificate, expires: newExpiresDate, renew: true });
			} else {
				error(
					`Could not get new certificate for '${domain}'! Adding domain to blacklist for 24 hours. If you think this is a mistake, restart me.`
				);
				blacklist.push({ domain, time: time + 24 * 60 * 60 * 1000 });
			}
		}

		for (const certificate of certificates) {
			await Certificate.deleteMany({ domain: certificate.domain });
			await Certificate.create(certificate);
			info(`Stored new certificate for '${certificate.domain}'!`);
		}

		await Certificate.deleteMany({ expires: { $lt: Date.now() } });
	}
}
