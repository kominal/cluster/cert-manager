import { model, Schema } from 'mongoose';

const Certificate = model(
	'Certificate',
	new Schema(
		{
			domain: String,
			csr: String,
			key: String,
			cert: String,
			expires: Date,
			renew: Boolean,
		},
		{ minimize: false }
	)
);

export default Certificate;
