import { Document, model, Schema } from 'mongoose';

export interface Property {
	key: string;
	value: any;
}

export const PropertyDatabase = model<Document & Property>(
	'Property',
	new Schema(
		{
			key: String,
			value: Object,
		},
		{ minimize: false }
	)
);
