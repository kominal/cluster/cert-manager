import Docker from 'dockerode';

const docker = new Docker();

export async function getDomains() {
	const domains: string[] = [];
	const services = await docker.listServices();
	for (const service of services) {
		const serviceObject = docker.getService(service.ID);
		const inspect = await serviceObject.inspect();
		const labels = inspect.Spec.Labels;
		for (const label in labels) {
			const value = labels[label];

			const regex = new RegExp(/Host\(`(.*?)`\)/gi);
			let matches;
			while ((matches = regex.exec(value)) !== null) {
				const url = matches[1];
				if (!domains.find((domain) => domain === url)) {
					domains.push(url);
				}
			}

			const regexSNI = new RegExp(/HostSNI\(`(.*?)`\)/gi);
			while ((matches = regexSNI.exec(value)) !== null) {
				const url = matches[1];
				if (!domains.find((domain) => domain === url)) {
					domains.push(url);
				}
			}
		}
	}
	return domains;
}
